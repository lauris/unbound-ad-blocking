#!/bin/sh

OUTFILE="/etc/adservers.db"

{ { curl -s -f 'https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts';
    curl -s -f 'http://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&mimetype=plaintext';
    curl -s -f 'http://someonewhocares.org/hosts/hosts';
    curl -s -f 'https://raw.githubusercontent.com/tyzbit/hosts/master/data/tyzbit/hosts';
    curl -s -f 'https://rules.emergingthreats.net/fwrules/emerging-Block-IPs.txt';
    curl -s -f 'https://www.malwaredomainlist.com/hostslist/hosts.txt';
    curl -s -f 'https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/win10/spy.txt';
    curl -s -f 'http://sysctl.org/cameleon/hosts';
    curl -s -f 'https://hosts-file.net/ad_servers.txt';
} | sed s/#.*$// | tr -d '\r' | awk '/^127\.0\.0\.1/ { print $2 } /^0\.0\.0\.0/ { print $2 }' | sort | uniq | awk '{ print "local-zone: \"" $1 "\" redirect\nlocal-data: \"" $1 " A 127.0.0.1\""}'; } > ${OUTFILE}

if [ $? -eq 0 ]; then
   unbound-checkconf
else
   echo "FAILED to fetch and parse remote sources...!"
   exit 1
fi

if [ $? -eq 0 ]; then
    rcctl restart unbound
else
   echo "FAILED to reload unbound !"
fi
